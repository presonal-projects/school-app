import React from 'react';
import {
	StyleSheet,
	SafeAreaView,
	StatusBar,
	ScrollView,
	Linking,
	Alert,
} from 'react-native';
import { Entypo } from '@expo/vector-icons';

import colors from '../constants/colors';
import { RowItem, RowSeparator } from '../components/RowItem';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: StatusBar.currentHeight,
		backgroundColor: colors.offWhite,
	},
});

const openUrl = (url) => {
	Linking.openURL(url).catch(() => {
		Alert.alert('Sorry, something went wrong.', 'Please try again later.');
	});
};

export default () => {
	return (
		<SafeAreaView style={styles.container}>
			<StatusBar barStyle='dark-content' backgroundColor={colors.white} />
			<ScrollView>
				<RowItem
					text='Themes'
					onPress={() => alert('One todo!')}
					rightIcon={
						<Entypo name='chevron-right' size={20} color={colors.text} />
					}
				/>

				<RowSeparator />

				<RowItem
					text='Open Google'
					onPress={() => openUrl('https://google.com')}
					rightIcon={<Entypo name='export' size={20} color={colors.text} />}
				/>

				<RowSeparator />

				<RowItem
					text='Open React Native Official Website'
					onPress={() => openUrl('https://reactnative.dev')}
					rightIcon={<Entypo name='export' size={20} color={colors.text} />}
				/>
			</ScrollView>
		</SafeAreaView>
	);
};
