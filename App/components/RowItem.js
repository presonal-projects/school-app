import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

import colors from '../constants/colors';

const styles = StyleSheet.create({
	row: {
		paddingHorizontal: 20,
		paddingVertical: 16,
		marginHorizontal: 10,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: colors.offWhite,
	},
	title: {
		color: colors.text,
		fontSize: 16,
	},
	separator: {
		backgroundColor: colors.border,
		height: StyleSheet.hairlineWidth,
		marginHorizontal: 20,
	},
});

export const RowItem = ({ text, rightIcon, onPress }) => {
	return (
		<TouchableOpacity onPress={onPress} style={styles.row}>
			<Text style={styles.title}>{text}</Text>
			{rightIcon}
		</TouchableOpacity>
	);
};

export const RowSeparator = () => <View style={styles.separator} />;
