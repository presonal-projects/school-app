module.exports = {
	extends: ['handlebarlabs'],
	rules: {
		'react/jsx-props-no-spreading': 0,
		'react/jsx-curly-newline': 0,
		'react/jsx-indent': [2, 'tab'],
		'react/jsx-indent-props': [2, 'tab'],
	},
};
